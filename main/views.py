from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse

def index(request):
    return render(request, 'main/index.html', {})

def loginpage(request):
	return render(request, 'main/loginpage.html')
