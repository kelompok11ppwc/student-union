# Tugas 1 PPW Kelompok 11

## Anggota Kelompok
- Adelia Utami (1706984493)
- Muhamad Istiady Kartadibrata (1706025283)
- William Gates (1706040044)

## Status Aplikasi
[![pipeline status](https://gitlab.com/kelompok11ppwc/student-union/badges/master/pipeline.svg)](https://gitlab.com/kelompok11ppwc/student-union/commits/master)
[![coverage report](https://gitlab.com/kelompok11ppwc/student-union/badges/master/coverage.svg)](https://gitlab.com/kelompok11ppwc/student-union/commits/master)

## Link Heroku App
https://student-union-kelompok-11.herokuapp.com