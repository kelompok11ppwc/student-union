from django.conf.urls import url
from . import views

app_name = 'about'

urlpatterns = [
	url(r'about/$', views.testimonial_func, name='about'),
	url(r'create-post/$', views.create_post ,name='create-post'),
]