from django.test import TestCase, Client
from django.urls import resolve
from .views import testimonial_func, create_post
from .models import Testimonial
from .forms import TestimonialForm

class TestimonialTest(TestCase):
	def test_url_is_exist(self):
		response = Client().get('/about/')
		self.assertEqual(response.status_code, 200)
		
	def test_using_about_us_template(self):
		response = Client().get('/about/')
		self.assertTemplateUsed(response, 'about_us.html')
		
	def test_using_testimonial_func(self):
		found = resolve('/about/')
		self.assertEqual(found.func, testimonial_func)
		
	def test_using_create_post_func(self):
		found = resolve('/about/create-post/')
		self.assertEqual(found.func, create_post)
		
	def test_models_can_create_new_testimonial(self):
		new_testimonial = Testimonial.objects.create(
			testimonial_message='contoh testi',)
		counting_all_available_testimonial = Testimonial.objects.all().count()
		self.assertEqual(counting_all_available_testimonial, 1)
	
	def test_can_save_a_POST_request(self):
		response = self.client.post('/about/', data = {
			'testimonial_message':'save testi', 
		})
		counting_all_available_testimonial = Testimonial.objects.all().count()
		self.assertEqual(counting_all_available_testimonial, 1)
		
		self.assertEqual(response.status_code, 302)
		self.assertEqual(response['location'], '/about/')
		
		new_response = self.client.get('/about/')
		html_response = new_response.content.decode('utf8')
		self.assertIn('', html_response)
	
	def test_create_post(self):
		response = self.client.get('/create-post/')
		self.assertEqual(response.status_code, 200)
		response = self.client.post('/create-post/', data={'the_post': 'testing'})
		self.assertEqual(response.status_code, 200)
