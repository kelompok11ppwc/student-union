from django.shortcuts import render, render_to_response
from django.template import RequestContext, loader
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib import messages
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt

from .models import Testimonial
from .forms import TestimonialForm

import json

def testimonial_func(request):
	data_obj = Testimonial.objects.all()
	if (request.method == 'POST'):
		testimonial_form = TestimonialForm(request.POST)
		if (testimonial_form.is_valid()):
			data = Testimonial(
				testimonial_message = request.POST['testimonial_message'],
				)
			data.save()
			return HttpResponseRedirect('/about/')
	else:
		testimonial_form = TestimonialForm()
	return render(request, 'about_us.html', {'testimonial_form':testimonial_form, 'data_obj':data_obj})
	
@csrf_exempt
def create_post(request):
	if request.method == 'POST':
		post_text = request.POST.get('the_post')
		response_data = {}
		
		post = Testimonial(testimonial_message=post_text)
		post.save()
		
		response_data['result'] = 'Create post successful!'
		response_data['message'] = post_text
		return HttpResponse(json.dumps(response_data), content_type="application/json" )
	else:
		return HttpResponse(json.dumps({"nothing to see": "AAAAAAAAAAAAAAAarggg"}), content_type="application/json")