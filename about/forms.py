from django.core.validators import RegexValidator
from django import forms
from .models import Testimonial

class TestimonialForm(forms.ModelForm):
	testimonial_message = forms.CharField(widget=forms.Textarea(attrs={
		'id' 			: 'post-text',
		'class'			: 'form-control',
		'required'		: True,
		'placeholder'	: 'Write your testimonial here.',
	}), label='', max_length=1000)
	
	class Meta:
		model = Testimonial
		fields = ('testimonial_message',)