from django.urls import path
from . import views

app_name = 'events'
urlpatterns = [
    path('', views.events, name='events'),
    path('register/', views.register, name='register'),
    path('register/member/<str:event_site>/', views.member_regist_event, name='member_register'),
    path('register/google-member/<str:event_site>/', views.google_member_regist_event, name='google_member_register'),
    path('register/non-member/<str:event_site>/', views.non_member_regist_event, name='non_member_register'),
]
