from django.core.validators import RegexValidator
from django import forms
from .models import Event, NonMemberParticipant

class MemberRegistEventForm(forms.ModelForm):
    username = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'required': True,
    }))
    
    password = forms.CharField(widget=forms.PasswordInput(attrs={
        'class': 'form-control',
        'required': True,
    }))
    
    class Meta:
        model = Event
        fields = ('username', 'password')

class NonMemberRegistEventForm(forms.ModelForm):
    username = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'required': True,
    }), max_length=15)
    
    password = forms.CharField(widget=forms.PasswordInput(attrs={
        'class': 'form-control',
        'required': True,
    }), min_length=8,
        validators=[RegexValidator('^(\w+\d+|\d+\w+)+$',
        message="Password should be a combination of Alphabets and Numbers")])

    email = forms.EmailField(widget=forms.EmailInput(attrs={
        'class': 'form-control',
        'required': True,
    }), label='E-mail')
    
    class Meta:
        model = Event
        fields = ('username', 'password', 'email')
