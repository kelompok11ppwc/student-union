from django.contrib import admin
from .models import Event, NonMemberParticipant

admin.site.register(Event)
admin.site.register(NonMemberParticipant)
