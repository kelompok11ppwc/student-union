# Generated by Django 2.1.1 on 2018-10-18 09:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0003_auto_20181018_1555'),
    ]

    operations = [
        migrations.CreateModel(
            name='NonMemberParticipant',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('username', models.CharField(max_length=15)),
                ('email', models.EmailField(max_length=254)),
                ('password', models.CharField(max_length=15)),
            ],
        ),
        migrations.AddField(
            model_name='event',
            name='non_member_participants',
            field=models.ManyToManyField(to='events.NonMemberParticipant'),
        ),
    ]
