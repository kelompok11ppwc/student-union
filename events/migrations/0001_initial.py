# Generated by Django 2.1.1 on 2018-10-17 08:16

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('site', models.CharField(max_length=100)),
                ('name', models.CharField(max_length=100)),
                ('image_url', models.CharField(max_length=300)),
                ('details', models.TextField()),
                ('date', models.DateTimeField()),
            ],
            options={
                'verbose_name_plural': 'events',
            },
        ),
    ]
