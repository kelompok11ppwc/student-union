from datetime import date, datetime, timedelta
from django.test import Client, TestCase
from django.urls import resolve
from django.utils import timezone
from member.models import Member
from django.contrib.auth.models import User
from .models import NonMemberParticipant, Event

class NonMemberParticipantModelTest(TestCase):
    def test_string_representation(self):
        non_member_participant = NonMemberParticipant(username='username', email='username@example.com', password='username123')
        self.assertEqual(str(non_member_participant), non_member_participant.username)

    def test_verbose_name_plural(self):
        self.assertEqual(str(NonMemberParticipant._meta.verbose_name_plural), 'non member participants')

class EventModelTest(TestCase):
    def test_string_representation(self):
        event = Event(site='event-site', name='Event Name', image_url='', details='Details', date='2000-01-01')
        self.assertEqual(str(event), event.name)

    def test_verbose_name_plural(self):
        self.assertEqual(str(Event._meta.verbose_name_plural), 'events')

class EventViewsTest(TestCase):
    def test_events_views(self):
        response = self.client.get('/events/')
        self.assertEqual(response.status_code, 200)
    
    def test_register_views(self):
        response = self.client.get('/events/register/')
        self.assertEqual(response.status_code, 200)

    def test_member_regist_event_views(self):
        member = Member(name='Full Name', username='username', email='username@example.com', password='username123', birthdate='2000-01-01', address='Address')
        member.save()
        event = Event(site='event-site', name='Event Name', image_url='', details='Details', date='2000-01-01')
        event.save()
        response = self.client.get('/events/register/member/event-site/')
        response = self.client.post('/events/register/member/event-site/', data={'username': 'username', 'password': 'username123'})
        response = self.client.post('/events/register/member/event-site/', data={'username': 'username', 'password': 'wrongpassword'})
        self.assertEqual(response.status_code, 200)

    def test_non_member_regist_event_views(self):
        event = Event(site='event-site', name='Event Name', image_url='', details='Details', date='2000-01-01')
        event.save()
        response = self.client.get('/events/register/non-member/event-site/')
        response = self.client.post('/events/register/non-member/event-site/', data={'username': 'username', 'email': 'username@example.com','password': 'username123'})
        self.assertEqual(response.status_code, 200)
    
    def test_google_member_regist_event_views(self):
        event = Event(site='event-site', name='Event Name', image_url='', details='Details', date='2000-01-01')
        event.save()
        response = self.client.get('/events/register/google-member/event-site/')
        self.assertEqual(response.status_code, 302)
        user = User.objects.create_user(username='testuser', password='12345')
        login = self.client.login(username='testuser', password='12345')
        response = self.client.get('/events/register/google-member/event-site/')
        response = self.client.post('/events/register/google-member/event-site/')
        self.assertEqual(response.status_code, 200)
