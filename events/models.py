import datetime
from django.db import models
from django.utils import timezone
from member.models import Member
from django.contrib.auth.models import User

class NonMemberParticipant(models.Model):
    username = models.CharField(max_length=15, unique=True)
    email = models.EmailField(unique=True)
    password = models.CharField(max_length=15)

    class Meta:
        verbose_name_plural = 'non member participants'

    def __str__(self):
        return self.username

class Event(models.Model):
    site = models.CharField(max_length=100)
    name = models.CharField(max_length=100)
    image_url = models.CharField(max_length=300)
    details = models.TextField()
    date = models.DateTimeField()
    member_participants = models.ManyToManyField(Member, blank=True)
    google_member_participants = models.ManyToManyField(User, blank=True)
    non_member_participants = models.ManyToManyField(NonMemberParticipant, blank=True)

    class Meta:
        verbose_name_plural = 'events'

    def __str__(self):
        return self.name
