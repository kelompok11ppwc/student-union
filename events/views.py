from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from member.models import Member
from social_django.models import *
from django.views.decorators.csrf import csrf_exempt
from .models import Event, NonMemberParticipant
from .forms import MemberRegistEventForm, NonMemberRegistEventForm

def events(request):
	all_events = Event.objects.all()
	return render(request, 'events/events.html', {'all_events': all_events})

def register(request):
    return render(request, 'events/register.html', {})

def google_member_regist_event(request, event_site):
    event = get_object_or_404(Event, site=event_site)
    if (request.user.is_authenticated):
        if (request.method == 'POST'):
            event.google_member_participants.add(User.objects.filter(username=request.user.username)[0])
            event.save()
            return render(request, 'events/google_member_register.html', {'event': event, 'message': 'Registered!'})
        else:
            return render(request, 'events/google_member_register.html', {'event': event})
    else:
        return HttpResponseRedirect('/login/')

def member_regist_event(request, event_site):
    event = get_object_or_404(Event, site=event_site)
    if (request.method == 'POST'):
        regist_event_form = MemberRegistEventForm(request.POST)
        if (regist_event_form.is_valid()):
            for member in Member.objects.all():
                if member.username == request.POST['username'] and member.password == request.POST['password']:
                    event.member_participants.add(member)
                    event.save()
                    return render(request, 'events/member_register.html', {'event': event, 'regist_event_form': regist_event_form, 'message': 'Registered!'})
        return render(request, 'events/member_register.html', {'event': event, 'regist_event_form': regist_event_form, 'message': 'Wrong username or password'})
    else:
        regist_event_form = MemberRegistEventForm()
    return render(request, 'events/member_register.html', {'event': event, 'regist_event_form': regist_event_form})

def non_member_regist_event(request, event_site):
    event = get_object_or_404(Event, site=event_site)
    if (request.method == 'POST'):
        regist_event_form = NonMemberRegistEventForm(request.POST)
        if (regist_event_form.is_valid()):
            data = NonMemberParticipant(
                username = request.POST['username'],
                email = request.POST['email'],
                password = request.POST['password'],
                )
            data.save()

            event.non_member_participants.add(data)
            event.save()
            return render(request, 'events/non_member_register.html', {'event': event, 'regist_event_form': regist_event_form, 'message': 'Registered!'})
    else:
        regist_event_form = NonMemberRegistEventForm()
    return render(request, 'events/non_member_register.html', {'event': event, 'regist_event_form': regist_event_form})
