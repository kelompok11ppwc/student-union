from django.shortcuts import render, render_to_response
from django.template import RequestContext, loader
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.urls import reverse
from .models import Member
from .forms import MemberForm

def member(request):
    if (request.method == 'POST'):
        member_form = MemberForm(request.POST)
        if (member_form.is_valid()):
            data = Member(
                name = request.POST['name'], 
                username = request.POST['username'],
                email = request.POST['email'],
                password = request.POST['password'],
                birthdate = request.POST['birthdate'],
                address = request.POST['address'],
                )
            data.save()
            return HttpResponseRedirect('/member/success/')
    else:
        member_form = MemberForm()
    return render(request, 'member/member.html', {'member_form':member_form,})

def success(request):
    return render(request, 'member/success.html', {})
