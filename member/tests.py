from django.test import TestCase, Client
from django.urls import resolve
from .views import member
from .models import Member
from .forms import MemberForm

class registrationTest(TestCase):
	def test_url_is_exist(self):
		response = Client().get('/member/')
		self.assertEqual(response.status_code, 200)
		
	def test_using_member_template(self):
		response = Client().get('/member/')
		self.assertTemplateUsed(response, 'member/member.html')
		
	def test_using_member_func(self):
		found = resolve('/member/')
		self.assertEqual(found.func, member)
		
	def test_models_can_create_new_member(self):
		new_member = Member.objects.create(
			name='adel', 
			username='adeliaut', 
			email='adel@gmail.com',
			password='tuwaga123',
			birthdate='2000-01-01',
			address='depok',)
		counting_all_available_member = Member.objects.all().count()
		self.assertEqual(counting_all_available_member, 1)
	
	def test_can_save_a_POST_request(self):
		response = self.client.post('/member/', data = {
			'name':'adelia utami', 
			'username':'adeliaut', 
			'email':'adel@gmail.com',
			'password':'tuwaga123',
			'birthdate':'2000-01-21',
			'address':'depok',
		})
		counting_all_available_member = Member.objects.all().count()
		self.assertEqual(counting_all_available_member, 1)
		
		self.assertEqual(response.status_code, 302)
		self.assertEqual(response['location'], '/member/success/')
		self.client.get('/member/success/')
		
		new_response = self.client.get('/member/')
		html_response = new_response.content.decode('utf8')
		self.assertIn('', html_response)

		member = Member.objects.get(name='adelia utami')
		self.assertEqual(str(member), 'adeliaut')
