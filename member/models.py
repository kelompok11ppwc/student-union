from django.db import models

class Member(models.Model):
    name = models.CharField(max_length=50)
    username = models.CharField(max_length=15, unique=True)
    email = models.EmailField(unique=True)
    password = models.CharField(max_length=15)
    birthdate = models.DateField()
    address = models.CharField(max_length=300)

    def __str__(self):
        return self.username
