from django.core.validators import RegexValidator
from django import forms
from .models import Member

class MemberForm(forms.ModelForm):
    name = forms.CharField(widget=forms.TextInput(attrs={
        'class':'form-control',
        'required':True,
    }), label='Full Name', max_length=50)
    
    username = forms.CharField(widget=forms.TextInput(attrs={
        'class':'form-control',
        'required':True,
    }), max_length=15)
    
    email = forms.EmailField(widget=forms.EmailInput(attrs={
        'class':'form-control',
        'required':True,
    }), label='E-mail')
    
    password = forms.CharField(widget=forms.PasswordInput(attrs={
        'class':'form-control',
        'required':True,
    }), min_length=8,
        validators=[RegexValidator('^(\w+\d+|\d+\w+)+$',
        message="Password should be a combination of Alphabets and Numbers")])
    
    birthdate = forms.DateField(widget=forms.DateInput(attrs={
        'class':'form-control',
        'required':True,
        'placeholder':'YYYY-MM-DD',
    }), label='Birth Date')

    address = forms.CharField(widget=forms.Textarea(attrs={
        'class':'form-control',
        'required':True,
    }), max_length=300)
    
    class Meta:
        model = Member
        fields = ('name', 'username', 'email', 'password', 'birthdate', 'address')
