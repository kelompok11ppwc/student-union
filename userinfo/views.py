from django.shortcuts import render
from events.models import Event
from django.http import JsonResponse, HttpResponseRedirect
from django.contrib.auth.models import User
import datetime

def userpage(request):
	if (request.user.is_authenticated):
		registered_events = Event.objects.filter(google_member_participants=request.user)
		count = registered_events.count()
		return render(request, 'userinfo.html', {'registered_events': registered_events, 'count': count})
	return render(request,'userinfo.html', {'count': 0})

def get_events(request):
    if (request.user.is_authenticated):
        all_events = {}
        for event in Event.objects.filter(google_member_participants=User.objects.get(username=request.user.username)):
            all_events[event.site] = {'name': event.name,
                                    'image_url': event.image_url,
                                    'details': event.details,
                                    'date': event.date.strftime("%Y-%m-%d")}
        return JsonResponse({'data': all_events})
    return HttpResponseRedirect('/login/')
