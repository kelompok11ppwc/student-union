from django.test import TestCase
from django.urls import resolve
from django.contrib.auth.models import User
from events.models import Event
from . import views

# Create your tests here.
class UserPageTest(TestCase):
	def test_userpage_url(self):
		response = self.client.get('/userpage/')
		self.assertEqual(response.status_code, 200)

	def test_userpage_func(self):
		found = resolve('/userpage/')
		self.assertEqual(found.func, views.userpage)

class RegisteredEventsTest(TestCase):
	def test_registered_events(self):
		user = User.objects.create_user(username='testuser', password='12345')
		login = self.client.login(username='testuser', password='12345')
		response = self.client.get('/userpage/')
		self.assertEqual(response.status_code, 200)

class GetEventsTest(TestCase):
	def test_get_events(self):
		response = self.client.get('/userpage/')
		self.assertEqual(response.status_code, 200)
		response = self.client.get('/userpage/get-events/')
		self.assertEqual(response.status_code, 302)

		user = User.objects.create_user(username='testuser', password='12345')
		user.save()
		login = self.client.login(username='testuser', password='12345')
		event = Event(site='event-site', name='Event Name', image_url='', details='Details', date='2000-01-01')
		event.save()
		event.google_member_participants.add(user)
		
		response = self.client.get('/userpage/')
		self.assertEqual(response.status_code, 200)
		response = self.client.get('/userpage/get-events/')
		self.assertEqual(response.status_code, 200)
