from django.urls import path
from . import views

app_name = 'userinfo'
urlpatterns = [
    path('', views.userpage, name='userpage'),
    path('get-events/', views.get_events, name='get_events')
]
