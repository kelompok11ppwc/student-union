from django.core.validators import RegexValidator
from django import forms
from member.models import Member
from .models import News

class LikeForm(forms.ModelForm):
    username = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'required': True,
    }))

    password = forms.CharField(widget=forms.PasswordInput(attrs={
        'class': 'form-control',
        'required': True,
    }))

    class Meta:
        model = News
        fields = ('username', 'password')
