from django.test import TestCase
from member.models import Member
from .models import News

class NewsModelsTests(TestCase):
    def test_string_representation(self):
        news = News(title='News Title', image_url='', details='Details', pub_date='2000-01-01')
        news.save()
        self.assertEqual(str(news), news.title)

class NewsViewsTests(TestCase):
    def test_news_views(self):
        response = self.client.get('/news/')
        self.assertEquals(response.status_code, 200)

    def test_detail_views(self):
        member = Member(name='Full Name', username='username', email='username@example.com', password='username123', birthdate='2000-01-01', address='Address')
        member.save()
        news = News(site='news-site', title='News Title', image_url='', details='Details', pub_date='2000-01-01')
        news.save()
        response = self.client.get('/news/news-site/')
        response = self.client.post('/news/news-site/', data={'username': 'username', 'password': 'username123'})
        self.assertEqual(news.likers.all().count(), 1)
        response = self.client.post('/news/news-site/', data={'username': 'username', 'password': 'wrongpassword'})
        self.assertEqual(response.status_code, 200)
