import datetime
from django.db import models
from django.utils import timezone
from member.models import Member

class News(models.Model):
    site = models.CharField(max_length=100)
    title = models.CharField(max_length=100)
    image_url = models.CharField(max_length=300)
    details = models.TextField()
    pub_date = models.DateTimeField('date published')
    likers = models.ManyToManyField(Member, blank=True)

    class Meta:
        verbose_name_plural = "news"

    def __str__(self):
        return self.title
