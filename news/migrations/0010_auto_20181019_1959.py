# Generated by Django 2.0.3 on 2018-10-19 12:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0009_auto_20181018_1254'),
    ]

    operations = [
        migrations.AlterField(
            model_name='news',
            name='likers',
            field=models.ManyToManyField(blank=True, to='member.Member'),
        ),
    ]
