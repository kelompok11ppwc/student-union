from django.shortcuts import render, render_to_response, get_object_or_404
from django.template import RequestContext, loader
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.urls import reverse
from member.models import Member
from .models import News
from .forms import LikeForm

def news(request):
	all_news = News.objects.all()
	return render(request, 'news/news.html', {'all_news': all_news})

def detail(request, news_site):
    news = get_object_or_404(News, site=news_site)
    if (request.method == 'POST'):
        like_form = LikeForm(request.POST)
        if (like_form.is_valid()):
            for member in Member.objects.all():
                if member.username == request.POST['username'] and member.password == request.POST['password']:
                    news.likers.add(member)
                    news.save()
                    return render(request, 'news/detail.html', {'news': news, 'likers': news.likers.all(), 'like_form': like_form, 'message': 'Like added!'})
        return render(request, 'news/detail.html', {'news': news, 'likers': news.likers.all(), 'like_form': like_form, 'message': 'Wrong username or password'})
    else:
        like_form = LikeForm()
    return render(request, 'news/detail.html', {'news': news, 'likers': news.likers.all(), 'like_form': like_form})
